# -*- coding: utf-8 -*-
"Parse icalendar elements into events"
# Author: Óscar Nájera
# License: GPL-3

from datetime import datetime, timedelta
from typing import Dict, Iterable, List

from dateutil import tz
from dateutil.rrule import rrulestr
from icalendar import Calendar  # type: ignore
from pytz import utc

# inspiration from
# https://www.nylas.com/blog/calendar-events-rrules/


def ensure_list(x) -> List:
    "Make X a list."
    if x is None:
        return []
    if not isinstance(x, list):
        return [x]
    return x


def put_tz(date_time):
    "Given date or datetime enforce transformation to localtime"
    if not hasattr(date_time, "hour"):
        return datetime(
            year=date_time.year,
            month=date_time.month,
            day=date_time.day,
            tzinfo=tz.tzlocal(),
        )
    return date_time.astimezone(tz.tzlocal())


def rrule_cleanup(rrule_conf):
    "Repetition rule needs to respect some constrains, return clean string"
    if until := rrule_conf.get("UNTIL"):
        if not hasattr(until[0], "hour"):
            until[0] = put_tz(until[0])
        rrule_conf["UNTIL"][0] = until[0].astimezone(tz.UTC)

    return rrule_conf.to_ical().decode("utf-8")


def rrule(org_event, exceptions=None):
    "Create event repetition rule"

    rule = rrulestr(org_event.entry["RRULE"], dtstart=org_event.dtstart, forceset=True)

    for dates in ensure_list(org_event.entry.get("EXDATE")):
        for date in dates.dts:
            rule.exdate(put_tz(date.dt))

    for date in ensure_list(exceptions):
        dt = put_tz(date.dt)
        dt = dt.replace(hour=org_event.dtstart.hour, minute=org_event.dtstart.minute)
        rule.exdate(dt)

    return rule


def date_block(event, start, end, exceptions=None):
    "Generate active start dates the event has"
    if "RRULE" in event.entry:
        rule = rrule(event, exceptions)
        yield from rule.between(after=start, before=end)

    elif start < event.dtstart < end:
        yield event.dtstart


class Event:
    """Event"""

    def __init__(self, event):
        self.entry = event
        self.etype = None

        self.heading = str(event.get("SUMMARY"))

        if self.heading.startswith("DL: "):
            self.heading = self.heading[4:]
            self.etype = "DL"
        if self.heading.startswith("S: "):
            self.heading = self.heading[3:]
            self.etype = "S"

        self.dtstart = put_tz(event["DTSTART"].dt)
        if "DTEND" in event:
            self.dtend = put_tz(event["DTEND"].dt)
            self.duration = self.dtend - self.dtstart
        elif "DURATION" in event:
            self.duration = event["DURATION"].dt
        else:
            self.duration = timedelta(3600)

        if "RRULE" in event:
            self.entry["RRULE"] = rrule_cleanup(event["RRULE"])

        self.dates = []

        self.description = (
            event.get("DESCRIPTION", "").replace(" \n", "\n").replace("*", "-")
        )

    @property
    def properties(self):
        "Extract relevant properties"
        properties = ("LOCATION", "UID", "RRULE")
        for prop in properties:
            if value := self.entry.get(prop, "").strip():
                if prop == "UID":
                    prop = "ID"
                yield prop, value

        for comp in self.entry.subcomponents:
            if comp.name == "VALARM":
                trigger = int(-1 * comp["TRIGGER"].dt.total_seconds() / 60)
                yield "APPT_WARNTIME", str(trigger)

    @property
    def tags(self):
        "Tags"

        for tag in ensure_list(self.entry.get("CATEGORIES")):
            for cat in tag.cats:
                yield cat.replace(" ", "_").replace("-", "_")

        if "RRULE" in self.entry or "RECURRENCE-ID" in self.entry:
            yield "rrule"

    def date_block(self, start, end, exceptions=None):
        "Evaluate which active dates the event has"
        self.dates = list(date_block(self, start, end, exceptions))
        return self.dates

    def time_interval(self):
        "(start, end, use HH:MM) in time_zone. Drop a full_day and skip time in org"
        full_day = (
            self.duration.total_seconds() % 86400 == 0
            and self.dtstart.hour == 0
            and self.dtstart.minute == 0
        )
        for start in self.dates:
            end_time = start + self.duration
            if full_day:
                end_time -= timedelta(seconds=86400)
            yield start, end_time, not full_day


def changev(evlist, start: datetime, end: datetime) -> List[Event]:
    "Clean repeating occurrences that have a specific event change"
    mods = [e.entry.get("RECURRENCE-ID") for e in evlist]

    if len(evlist) == 1 or None not in mods:
        return evlist

    base_ind = mods.index(None)
    mods.pop(base_ind)

    base = evlist.pop(base_ind)
    base.date_block(start, end, mods)

    return ([base] if base.dates else []) + evlist


def gen_events(events: Iterable[Event], ahead: int, back: int) -> Iterable[Event]:
    "Filter events into interval [today-back;today+ahead]"
    regular_events: Dict[str, List[Event]] = {}
    now = datetime.now(utc)
    start = now - timedelta(back)
    end = now + timedelta(ahead)
    for event in events:
        if event.date_block(start, end):
            regular_events.setdefault(event.entry.get("UID"), []).append(event)

    for evlist in regular_events.values():
        yield from changev(evlist, start, end)


def parse_ical(ical: Iterable[str]) -> Iterable[Event]:
    "Generate Events from each ical text"
    for elements in map(Calendar.from_ical, ical):
        for entry in elements.walk():
            if entry.name == "VEVENT":
                yield Event(entry)


def agenda_output(calendars, formatter, ahead, back):
    "Calendars into formatted events"
    return "\n\n".join(map(formatter, gen_events(parse_ical(calendars), ahead, back)))
