# -*- coding: utf-8 -*-
r"""
Generate Org-Mode agenda file from ical web sources
===================================================

Goal of the script
"""
# Created: Fri Jan 11 22:54:35 2019
# Author: Óscar Nájera
# License:GPL-3

import argparse
import asyncio
import configparser
import hashlib
import logging
import os
import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Optional

from aiohttp import BasicAuth, ClientSession

from org_agenda import org
from org_agenda.events import agenda_output

LOGGER = logging.getLogger("Org_calendar")
LOGGER.addHandler(logging.StreamHandler(sys.stderr))


async def passwordstore(address: str) -> str:
    """Get password from passwordstore address"""

    process = await asyncio.create_subprocess_shell(
        f"pass show {address}", stdout=asyncio.subprocess.PIPE
    )
    stdout, _ = await process.communicate()

    if process.returncode == 0:
        return stdout.decode("utf-8").split()[0]

    raise ValueError(f"Unknown password address: {address}")


def read_file(filepath: Path) -> Optional[str]:
    "Read contents calendar file if present"
    if filepath.exists():
        with filepath.open(encoding="utf-8") as txt:
            return txt.read()
    return None


@dataclass
class Resource:
    "Tracks a calendar resource"
    url: str
    username: str = ""
    password_path: str = ""

    async def auth(self):
        "Auth object when needed."
        if self.username and self.password_path:
            password = await passwordstore(self.password_path)
            return BasicAuth(self.username, password)
        return None

    def target_file(self) -> Path:
        "Temporary file with content"
        hash_url = hashlib.sha1(self.url.encode("UTF-8")).hexdigest()[:10]
        return Path(f"/tmp/agenda-{hash_url}")


def url_resources(config: configparser.ConfigParser):
    "Evaluate calendar resources from config"
    for section in config.sections():
        for entry in config[section].get("calendars", "").split():
            yield Resource(
                url=config[section]["url"].format(entry),
                username=config[section].get("user"),
                password_path=config[section].get("passwordstore"),
            )


async def download(source: Resource, session: ClientSession, force: bool) -> str:
    "Return calendar from download or cache"
    if not force and (cached := read_file(source.target_file())):
        return cached

    LOGGER.info("Downloading from: %s", source.url)
    auth = await source.auth()
    async with session.get(source.url, auth=auth) as reply:
        if reply.status == 200:
            text = await reply.text()
            with open(source.target_file(), "w", encoding="utf-8") as txt:
                txt.write(text)
            return text

        raise ValueError(f"Could not get calendar: {source.url}\n{reply}")


async def get_resources(resources, force: bool):
    "RESOURCE are returned given the FORCE download"
    async with ClientSession() as session:
        cal = [download(source, session, force) for source in resources]
        return await asyncio.gather(*cal)


def get_config(filepath) -> configparser.ConfigParser:
    "parse config file"

    config = configparser.ConfigParser()

    config_file = os.path.expanduser(filepath)
    LOGGER.info("Reading config from: %s", config_file)
    config.read([config_file])

    return config


def parse_arguments():
    "Parse CLI"
    parser = argparse.ArgumentParser(description="Translate CalDav Agenda to orgfile")
    parser.add_argument(
        "-c",
        "--config-file",
        default=Path.home() / ".config" / "calendars.conf",
        help="Configuration file",
    )
    parser.add_argument(
        "-f", "--force", help="Force Download of Caldav files", action="store_true"
    )
    parser.add_argument("-r", "--url", help="force direct download from url no auth")
    parser.add_argument(
        "-o",
        "--format",
        default="lisp",
        choices=["lisp", "org"],
        help="Output format in default elisp or orgmode",
    )
    parser.add_argument("-v", "--verbose", action="count", default=0)

    return parser.parse_args()


def main():
    "Transform Calendars"
    args = parse_arguments()

    log_level = logging.INFO if args.verbose > 0 else logging.WARNING
    LOGGER.setLevel(log_level)

    config = get_config(args.config_file)

    resources = [Resource(url=args.url)] if args.url else url_resources(config)
    calendars = asyncio.run(get_resources(resources, args.force))

    ahead = int(config["DEFAULT"].get("ahead", "50"))
    back = int(config["DEFAULT"].get("back", "14"))
    formatter = {"lisp": org.elisp, "org": org.string}[args.format]

    print(agenda_output(calendars, formatter, ahead, back))
