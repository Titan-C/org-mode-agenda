# -*- coding: utf-8 -*-
r"""
Convert Caldav to org-contacts
==============================
"""
# Author: Óscar Nájera
# License: GPL-3
# Inspired https://gist.github.com/tmalsburg/9747104

from itertools import starmap


def orgdatetime(datestamp, time=True) -> str:
    """Timezone aware datetime to YYYY-MM-DD DayofWeek HH:MM str in localtime."""
    hours = " %H:%M" if time else ""
    return datestamp.strftime(f"<%Y-%m-%d %a{hours}>")


def org_interval(start, end, skip_time) -> str:
    "Write org interval"
    if start == end:
        return orgdatetime(start, skip_time)
    return f"{orgdatetime(start, skip_time)}--{orgdatetime(end, skip_time)}"


def date_type(event_node):
    "Prefix for date of deadline, schedule, none"
    if event_node.etype == "DL":
        return "DEADLINE: "
    if event_node.etype == "S":
        return "SCHEDULED: "
    return ""


def dates(event_node):
    "All dates formated in org syntax"
    return "\n".join(starmap(org_interval, event_node.time_interval()))


def tags(tag_list):
    "Tags"
    if _tags := ":".join(tag_list):
        return f"  :{_tags}:"
    return ""


def property_box(node):
    "Property box"
    props = "\n".join(f":{k}: {v}" for k, v in node.properties)
    return f""":PROPERTIES:\n{props}\n:END:\n""" if props else ""


def string(node):
    "string representation of org entry"
    if datep := date_type(node):
        info = f"{datep}{dates(node)}\n{property_box(node)}"
    else:
        info = f"{property_box(node)}{dates(node)}\n"

    return (f"* {node.heading}{tags(node.tags)}\n{info}{node.description}").strip()


def elist(iterable):
    "Convert to lisp list"
    return f"({' '.join(iterable)})"


def plist(pairs):
    "Convert to lisp property list"
    return "(" + " ".join(f':{k} "{v}"' for k, v in pairs) + ")"


def elisp(node):
    "Emacs Lisp representation of entry"
    return f"""(:heading \"{node.heading}\"
    :tags {elist(node.tags)}
    :properties {plist(node.properties)}
    :dates \"{dates(node)}\"{" :event-type \"node.etype\"" if node.etype else ""}
    :description \"{node.description.strip()}\")"""
